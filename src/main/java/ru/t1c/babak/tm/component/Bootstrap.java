package ru.t1c.babak.tm.component;

import ru.t1c.babak.tm.api.ICommandController;
import ru.t1c.babak.tm.api.ICommandRepository;
import ru.t1c.babak.tm.api.ICommandService;
import ru.t1c.babak.tm.constant.ArgumentConst;
import ru.t1c.babak.tm.constant.TerminalConst;
import ru.t1c.babak.tm.controller.CommandController;
import ru.t1c.babak.tm.repository.CommandRepository;
import ru.t1c.babak.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);


    public void run(final String[] args) {
        if (runArgument(args)) System.exit(0);
        commandController.showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND: ");
            final String command = scanner.nextLine();
            runCommand(command);
        }
    }

    public boolean runArgument(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        runArgument(arg);
        return true;
    }

    public void runArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showSystemInfo();
                break;
            default:
                commandController.showErrorArgument(arg);
                break;
        }
    }

    public void runCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.EXIT:
                close();
                break;
            default:
                commandController.showErrorCommand(command);
                break;
        }
    }

    public void close() {
        System.exit(0);
    }

}
