package ru.t1c.babak.tm;

import ru.t1c.babak.tm.api.ICommandController;
import ru.t1c.babak.tm.api.ICommandRepository;
import ru.t1c.babak.tm.api.ICommandService;
import ru.t1c.babak.tm.component.Bootstrap;
import ru.t1c.babak.tm.constant.ArgumentConst;
import ru.t1c.babak.tm.constant.TerminalConst;
import ru.t1c.babak.tm.model.Command;
import ru.t1c.babak.tm.repository.CommandRepository;
import ru.t1c.babak.tm.service.CommandService;
import ru.t1c.babak.tm.util.InformationData;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Scanner;

/**
 * Simple task manager application.
 *
 * @author Artem Babak
 */
public final class Application {

    public static void main(final String[] args) {
        new Bootstrap().run(args);
    }

}
