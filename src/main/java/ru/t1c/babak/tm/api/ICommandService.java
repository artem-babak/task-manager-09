package ru.t1c.babak.tm.api;

import ru.t1c.babak.tm.model.Command;

public interface ICommandService {
    Command[] getTerminalCommands();
}
